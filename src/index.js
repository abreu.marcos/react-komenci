import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './containers/app';

// ### Static Global Assets
// ----------------------------------------------------------------------------
import './assets/images/favicon.ico'; // webpack to load favicon.ico
import './assets/styles/app.scss'; // root SASS style tree

// ### Routes
// ----------------------------------------------------------------------------
import { BrowserRouter as Router } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

// ### Browser History
const history = createBrowserHistory();


// ### Application Bootstrap
// ----------------------------------------------------------------------------
const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Router history={history}>
        <Component />
      </Router>
    </AppContainer>
  , document.getElementById('app') );
};

render(App);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./containers/app', () => { render(App); });
}
