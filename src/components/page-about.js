import React from 'react';

const PageAbout = () => {
  return (
    <div className="expanded row column">
      <header>
        <h1>React komenci</h1>
      </header>

      <section>
        <p>Basic structure to start your react project</p>
      </section>
    </div>
  );
};

export default PageAbout;
