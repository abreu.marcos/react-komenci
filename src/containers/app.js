import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link, Switch, Route } from 'react-router-dom';

import PageHome from '../components/page-home';
import PageAbout from '../components/page-about';
import PageNotFound from '../components/page-not-found';

import AppHeader from '../components/app-header';
import AppFooter from '../components/app-footer';

/*
 * App Component - Root component of our application
 *                 include here the shared functionality of the app
 */
class App extends Component {
  // component properties
  static propTypes = {
    children: PropTypes.element             // element children
  }

  // application context properties
  static contextTypes = {
    router: PropTypes.object
  }

  // Render App Shared Components
  render() {
    return (
      <div>
        <AppHeader />

        <nav id="app-nav" className="row column">
          <ul className="menu small-12 columns">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
          </ul>
        </nav>

        <section className="app-content">
          <Switch>
            <Route path="/" exact component={PageHome} />
            <Route path="/about" component={PageAbout} />
            <Route component={PageNotFound} />
          </Switch>
        </section>

        <AppFooter />
      </div>
    );
  }
}

export default App;
