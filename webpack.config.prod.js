// For info on how we're generating bundles with hashed filenames for cache busting: https://medium.com/@okonetchnikov/long-term-caching-of-static-assets-with-webpack-1ecb139adb95#.w99i89nsz
import fs from 'fs';
import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackMd5Hash from 'webpack-md5-hash';
import HtmlWebpackPlugin from 'html-webpack-plugin';

// Define the desired environment variables
const EnvironmentVariables = new webpack.EnvironmentPlugin({
  NODE_ENV: 'production',  // use 'production' unless process.env.NODE_ENV is defined
  DEBUG: false, // use false unless process.env.DEBUG is defined
  __DEV__: false // use false unless process.env.__DEV__ is defined
});

// read the presets and make sure it adds { modules: false } for ....
const babelConfigStr = fs.readFileSync(`${__dirname}/.babelrc`, 'utf8');
const babelConfig = JSON.parse(babelConfigStr);
const jsPresets = ['env', 'es2015', 'es2016', 'es2017', 'latest'];
let babelPresets = babelConfig.presets;
babelPresets = babelPresets.map(preset => {
  if (jsPresets.includes(preset)) {
    return [preset, { modules: false }];
  }
  return preset;
});

// Generate an external css file with a hash in the filename
const extractCSS = new ExtractTextPlugin({
  filename: 'css/[name].[contenthash].css'
});

export default {
  // Development Tools source code strategy
  // NOTE: https://webpack.github.io/docs/build-performance.html#sourcemaps and
  //       https://webpack.github.io/docs/configuration.html#devtool
  devtool: 'source-map',

  // Only outputs warnings and errors to the console, but not info messages
  // noInfo: true,

  entry: [
    // Include babel polyfill for browser compatibility with the JavaScript new specs
    'babel-polyfill',

    // Starting point of our js application
    './src/index'
  ],

  // Necessary per https://webpack.github.io/docs/testing.html#compile-and-test
  target: 'web',

  // Application output configuration
  output: {
    // Output folder path
    path: `${__dirname}/dist/`,

    // Public url path
    publicPath: '/',

    // Bundled js file
    filename: 'js/[name].[chunkhash].js',

    // Bundled js chunks
    chunkFilename: 'js/[name].[chunkhash].js',
  },

  stats: "errors-only",

  // Plugins used to enhance the source encoding
  plugins: [

    // Hash the files using MD5 so that their names change when the content changes.
    new WebpackMd5Hash(),

    // Tells React to build in prod mode. https://facebook.github.io/react/downloads.html
    EnvironmentVariables,

    // Extract CSS content into a separate file
    extractCSS,

    // Generate HTML file that contains references to generated bundles. See here for how this works: https://github.com/ampedandwired/html-webpack-plugin#basic-usage
    new HtmlWebpackPlugin({
      // Relative path to the template file
      template: 'public/index.html',

      // Minification options for the generated template file
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
      },

      // Inject all assets into the given template at the bottom of the body block
      inject: true,

      // Note that you can add custom options here if you need to handle other custom logic in index.html
      // To track JavaScript errors via TrackJS, sign up for a free trial at TrackJS.com and enter your token below.
      trackJSToken: ''
    }),

    // Minify JS
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: { warnings: true }
    }),

    // Adds a comment banner to the top of generated assets
    new webpack.BannerPlugin({
      banner: "hash:[hash], chunkhash:[chunkhash], name:[name]"
    })
  ],

  // Webpack processing rules
  module: {
    rules: [
      { // scripts
        test: /\.js$/,
        include: path.join(__dirname, 'src'),
        exclude: path.join(__dirname, 'tools'),
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: babelPresets
            }
          }
        ]
      },
      { // fonts - EOT format
        test: /\.eot(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              debug: true,
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      },
      { // fonts - WOOF format
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              debug: true,
              limit: 10000,
              mimetype: 'application/font-woff',
              name: 'fonts/[name].[ext]'
            },
          }
        ]
      },
      { // fonts - TTF format
        test: /\.ttf(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              debug: true,
              limit: 10000,
              mimetype: 'application/octet-stream',
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      },
      { // fonts - SVG format
        test: /\.svg(\?v=\d+.\d+.\d+)?$/,
        include: path.join(__dirname, 'src', 'assets', 'fonts'),
        use: [
          {
            loader: 'file-loader',
            options: {
              debug: true,
              limit: 10000,
              mimetype: 'image/svg+xml',
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      },
      { // images
        test: /\.(jpe?g|png|gif|svg)$/i,
        include: path.join(__dirname, 'src'),
        exclude: path.join(__dirname, 'src', 'assets', 'fonts'),
        use: [
          {
            loader: 'file-loader',
            options: {
              debug: true,
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      { // ico files - usually just for the favicon.ico
        test: /\.ico$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              debug: true,
              name: '[name].[ext]'
            }
          }
        ]
      },
      { // css files
        test: /(\.css|\.scss)$/,
        loader: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                debug: true,
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                debug: true,
                sourceMap: true,
                includePaths: [
                  path.resolve(__dirname, 'node_modules', 'foundation-sites', 'scss')
                ]
              }
            }
          ]
        })
      }
    ]
  }
};
